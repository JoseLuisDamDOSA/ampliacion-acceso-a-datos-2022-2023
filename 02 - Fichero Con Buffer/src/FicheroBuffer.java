import java.io.*;

public class FicheroBuffer {
	
	
	public static void escribir(String nombre,String[] tabla)
	{
		try 
		{
			File            f = new File(nombre);
			FileWriter     fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			
			
			// Escribir datos de la tabla
			for(int i=0;i<tabla.length;i++)
			{
				bw.write(tabla[i]);
				bw.newLine();
			}
			
			// Cerrar fichero
			bw.close();
			
		
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
		
	}

	
	public static void leer(String nombre,String[] tabla)
	{
		try 
		{
			File            f = new File(nombre);
			FileReader     fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String         linea;
			int            posicion = 0;
			
			
			linea = br.readLine();
			while (linea != null)
			{
				// Tratar la línea
				tabla[posicion] = linea;
				posicion++;
				// Lectura de siguiente linea
				linea = br.readLine();
			}
			
			// Cerrar el fichero
			br.close();
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
		
	}

	public static void main(String[] args)
	{
		// Creación de array de nombres String[] nombres = {"Pepe","Ana, "Raul", "Laura"}; 
		String[] nombres = new String[4];
		String[] nombresLeer = new String[20];
		nombres[0] = "Pepe";
		nombres[1] = "Ana";
		nombres[2] = "Raul";
		nombres[3] = "Laura";
		
		// escribir("Datos.txt",nombres);
		leer("Datos.txt",nombresLeer);
		nombres[3]= "slkdslkd";
	}

}
