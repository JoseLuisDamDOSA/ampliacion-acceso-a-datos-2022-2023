import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicheroMarca {
	
	
	public static void escribir(String nombre,String[] tabla)
	{
		try 
		{
			File            f = new File(nombre);
			FileWriter     fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			
			
			// Escribir datos de la tabla
			for(int i=0;i<tabla.length;i++)
			{
				bw.write(tabla[i]);
				bw.write(";");
			}
			
			// Cerrar fichero
			bw.close();
			
		
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
		
	}

	
	public static String[] leer(String nombre)
	{
		try 
		{
			File            f = new File(nombre);
			FileReader     fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String         linea;
			String[] tabla = new String[8];

			linea = br.readLine();
			while (linea != null)
			{
				// Tratar la línea separado con ; 
				tabla = linea.split(";");
				linea = br.readLine();
			}
			
			// Cerrar el fichero
			br.close();
			return tabla;
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	public static void imprimirTabla(String[] tabla)
	{
		// Como for(int i=0;i<tabla.length;i++) 
		//         System.out.println(tabla[i]);
		// Recorrido de la tabla con la instrucción for( : ) 
		for(String s : tabla)
			System.out.println(s);
	}

	public static void main(String[] args)
	{
		// Creación de array de nombres String[] nombres = {"Pepe","Ana, "Raul", "Laura"};
		/*
		String[] nombres = new String[4];
        nombres[0] = "Pepe";
		nombres[1] = "Ana";
		nombres[2] = "Raul";
		nombres[3] = "Laura";
		*/
		
		//escribir("Datos.txt",nombres);
		
		// Los datos del fichero están separados por como (;)
		// EXPLICACIÓN DE ARRAY COMO VALOR DE RETONRNO
		// Para poder devolver el array como retorno, en la función main
		// no se puede  definir  el array  como  new String[], ya que la
		// función leer vuelve a crear la tabla con new String[]. Por lo
		// que crearía dos veces con  nombres distintos, y el nombre del
		// array no puede cambiarse ya que es un valor constante.
		String[] nombresLeer = leer("Datos.txt");
		imprimirTabla(nombresLeer);
	}

}

