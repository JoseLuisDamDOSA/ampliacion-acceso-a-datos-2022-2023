import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class Principal {

	public static void leerCliente(String nombre,ArrayList<Cliente> lista)
	{
		try 
		{
			File            f = new File(nombre);
			FileReader     fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String         linea;
			String[]       tabla = new String[4];
			
			linea = br.readLine();
			while (linea != null)
			{
				// Tratar la línea separado con ; 
				tabla = linea.split(":");
				Cliente  c = new Cliente(tabla[0],tabla[1],tabla[2],Integer.parseInt(tabla[3]));
				lista.add(c);
				linea = br.readLine();
			}
			// Cerrar el fichero
			br.close();
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
		
	}
	
	public static void leerCompras(String nombre,ArrayList<Cliente> lista)
	{
		try 
		{
			File              f = new File(nombre);
			FileReader       fr = new FileReader(f);
			BufferedReader   br = new BufferedReader(fr);
			String           linea;
			String[]         tabla = new String[4];
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			linea = br.readLine();
			while (linea != null)
			{
				// Tratar la línea separado con : CodCompra:CodCliente:Comentario:Precio:Fecha 
				tabla = linea.split(":");
				
				Compra c = new Compra(tabla[0],tabla[2],Integer.parseInt(tabla[3]),dateFormat.parse(tabla[4]));
                for(int i=0;i<lista.size();i++)
				{
					if (lista.get(i).getClave().compareTo(tabla[1]) == 0 ) 
					{
						lista.get(i).getLista().add(c);
					}
				}
				linea = br.readLine();
			}
			// Cerrar el fichero
			br.close();
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
		catch(ParseException e) 
		{
			System.out.println(e.getMessage());
		}
		
	}
	
	public static void main(String[] args)
	{
		ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
		
		leerCliente("Clientes.txt",listaCliente);
		leerCompras("Compras.txt", listaCliente);
				
	}

}
