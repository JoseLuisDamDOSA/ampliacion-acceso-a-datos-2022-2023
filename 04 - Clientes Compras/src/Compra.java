import java.util.*;

public class Compra 
{
	private String clave;
	private String texto;
	private int    precio;
	private Date   fecha;
	
	public Compra(String clave, String texto, int precio, Date fecha) {
		this.clave = clave;
		this.texto = texto;
		this.precio = precio;
		this.fecha = fecha;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
	
	
	
		

}
