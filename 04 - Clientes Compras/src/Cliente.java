import java.util.*;
public class Cliente 
{
	private String clave;
	private String nombre;
	private String direccion;
	private int edad;
	private ArrayList<Compra> listaCompra;
	
	public String getClave() {
		return clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public ArrayList<Compra> getLista()
	{
		return listaCompra;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public int getEdad() {
		return edad;
	}
	
	public void setEdad(int edad) {
		this.edad= edad;
	}
	
	public Cliente(String clave, String nombre, String direccion, int edad) {
	
		this.clave = clave;
		this.nombre = nombre;
		this.direccion = direccion;
		this.edad = edad;
		listaCompra = new ArrayList<Compra>();
	}
	
	
	
}
