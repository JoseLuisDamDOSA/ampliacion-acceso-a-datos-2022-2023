
import java.sql.*; 

public class pruebaJDBC {

	public static void main(String[] args) 
	{
		try
		{
		
			// Carga driver de conexión
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			// 1. Conexión con JDBC
			Connection conexion = DriverManager.getConnection
					("jdbc:mysql://localhost/bdcolegio","root","12345");

			Statement sentencia = conexion.createStatement();
			                     
			
			ResultSet resultado = sentencia.executeQuery("SELECT * FROM alumno");
			while (resultado.next())
			{
				System.out.println("        A#: " + resultado.getString(1) +  "\t" +  
	                               "    Nombre: " + resultado.getString(2) + "\t" +   
						           "      Edad: " + resultado.getString(3));
			}
			resultado.close();
			sentencia.close();
			conexion.close();
		}
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}

}
